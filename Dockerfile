FROM node:7.7.3-alpine
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app/
RUN yarn

COPY . /app
ENV PORT 8080
EXPOSE 8080
CMD [ "yarn", "start" ]

