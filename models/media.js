const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Media = new Schema({
    image_id: Number,
    path: String,
    size: String,
    type: String,
    name: String,
    height: Number,
    width: Number
});

module.exports = mongoose.model('Media', Media);
