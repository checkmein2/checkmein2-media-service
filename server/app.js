const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const server = require('http').createServer(app);
const config = require('../config/config');
const mongoose = require('mongoose');
const winston = require('winston');

mongoose.connect(config.database);
app.use(express.static(__dirname + '/assets'));
app.use('/documentation', express.static('documentation'));
app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);

require('../api/health')(app);
const routes = require('../api/routes')(app);
server.listen(config.port, error => {
    if (error) {
        winston.error('error', error);
        winston.log('Info', 'APP CAN NOT BE STARTED');
    }
    const msg = `server is listening to ${config.port}`;
    winston.log('Info', msg);
});
