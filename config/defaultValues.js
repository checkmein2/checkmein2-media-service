const defaultImageDimension = {
    WIDTH: 500,
    HEIGHT: 500
};

module.exports = defaultImageDimension;
