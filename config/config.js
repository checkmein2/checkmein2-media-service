const config = {
    port: process.env.PORT || 3000,
    database: `mongodb://${process.env.MONGO_URL}/Media`
};

module.exports = config;
