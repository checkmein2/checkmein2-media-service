const multer = require('multer');
const Media = require('../models/media');
const mime = require('mime-types');
const winston = require('winston');
const fs = require('fs');
const Jimp = require('jimp');
const defaultImageDimension = require('../config/defaultValues');
winston.add(winston.transports.File, { filename: 'error.log', level: 'error' });

const generatePhotoName = file => {
    const extension = mime.extension(file.mimetype);
    const photName = `${Date.now()}.${extension}`;
    return photName;
};
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './uploads');
    },
    filename: (req, file, cb) => {
        cb(null, generatePhotoName(file));
    }
});
const upload = multer({ storage }).any();
const handleError = (err, res) => {
    res.send(err);
    winston.error('Error', err);
};
const sendPhoto = (path, type, res) => {
    const img = fs.readFileSync(path);
    res.contentType(type);
    res.send(img);
};
const handleImgResize = (width, height, path, res, newPath) => {
    const imagePath = newPath || path;
    Jimp.read(path, (error, image) => {
        if (error) {
            return handleError(error, res);
        }
        image.resize(width, height).write(imagePath);
    });
};
const getImgWithSize = (width, height, id, path, res, extension) => {
    Media.findOne({ image_id: id, width, height }, (err, photo) => {
        if (err) {
            return handleError(err, res);
        }
        if (photo === null) {
            const fileName = Date.now();
            const newPath = `uploads/${fileName}.${extension}`;
            const newPhoto = {
                image_id: id,
                path: newPath,
                name: fileName,
                type: extension,
                width,
                height
            };
            Jimp.read(path)
                .then(image => {
                    image.resize(width, height).write(newPath, () => {
                        new Media(newPhoto).save((error, media) => {
                            if (error) {
                                return handleError(error, res);
                            }
                            sendPhoto(media.path, media.type, res);
                        });
                    });
                })
                .catch(msg => handleError(msg, res));
        } else {
            sendPhoto(photo.path, photo.type, res);
        }
    });
};
const getphotoById = (req, res) => {
    const id = req.params.id;
    const width = parseInt(req.query.width, 10);
    const height = parseInt(req.query.height, 10);
    const mediaQuerry = Media.findOne({
        image_id: id,
        height: defaultImageDimension.HEIGHT,
        width: defaultImageDimension.WIDTH
    });
    mediaQuerry.exec((err, photo) => {
        if (err) {
            return handleError(err, res);
        }
        if (photo === null) {
            return handleError('Image not found', res);
        }
        if (width && height) {
            const path = photo._doc.path;
            const extension = mime.extension(photo._doc.type);
            getImgWithSize(width, height, id, path, res, extension);
        } else {
            sendPhoto(photo.path, photo.type, res);
        }
    });
};
const deletePhoto = (req, res) => {
    const id = req.params.id;
    Media.find({ image_id: id }, (err, photos) => {
        if (err) {
            return handleError(err, res);
        }
        if (photos.length === 0) {
            return handleError('Image not found', res);
        }
        photos.forEach(photo => {
            Media.findOneAndRemove({ _id: photo._id }, error => {
                if (error) {
                    return handleError(err, res);
                }
            });
            fs.unlink(photo.path, error => {
                if (error) {
                    return handleError(error, res);
                }
            });
        });
        res.send({
            status: '200',
            responseType: 'string',
            response: 'success'
        });
    });
};
const getAllPhotos = (req, res) => {
    Media.find((err, photos) => {
        if (err) {
            return handleError(err, res);
        }
        res.send(photos);
    });
};

const savePhoto = (req, res) => {
    upload(req, res, err => {
        if (err) {
            return handleError(err, res);
        }
        const file = req.files[0];
        if (!file) {
            const error = 'File not found';
            return handleError(error, res);
        }
        const width = defaultImageDimension.WIDTH;
        const height = defaultImageDimension.HEIGHT;
        handleImgResize(width, height, file.path, res);
        const photo = {
            image_id: Date.now(),
            path: file.path,
            name: file.filename,
            size: file.size,
            type: file.mimetype,
            width,
            height
        };
        new Media(photo).save((error, media) => {
            if (error) {
                return handleError(error, res);
            }
            res.send(media);
        });
    });
};
const publicActions = {
    savePhoto,
    getAllPhotos,
    deletePhoto,
    getphotoById
};
module.exports = publicActions;
