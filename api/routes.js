const mediaServiceControler = require('../repository/media-service.js');

module.exports = app => {
    /**
   * @api {get} '/media-service/photo/:param' Get photo by photo id
   * @apiName photo
   * @apiGroup Media Service
   *
   * @apiParam {Number} param Id photo.
   *
   * @apiSuccess {Object} A JSON Object with the url of the photo
   *
   */
    app.get('/media/photo/:id', (req, res) => {
        mediaServiceControler.getphotoById(req, res);
    });
    /**
   * @api {get} '/media-service/photos' Get all photos
   * @apiName photos
   * @apiGroup Media Service
   *
   * @apiSuccess {Object} A JSON Object with the all photos
   *
   */
    app.get('/media/photos', (req, res) => {
        mediaServiceControler.getAllPhotos(req, res);
    });

    /**
   * @api {delete} /media-service/photo/:id Delete photo
   * @apiName Delete photo
   * @apiGroup Media-Service
   *
   * @apiParam {Number} id The id of the photo.
   *
   * @apiSuccess {Object} status A JSON object of the status
   *
   */
    app.delete('/media/photo/:id', (req, res) => {
        mediaServiceControler.deletePhoto(req, res);
    });
    /**
   * @api {put} /media-service/photo/:id Update photo
   * @apiName Update photo
   * @apiGroup Media Service
   *
   * @apiParam {Number} id The id of the photo.
   *
   * @apiSuccess {Object} photo A JSON object with the url of the photo
   *
   */
    app.put('/media/photo/:id', (req, res) => {
        mediaServiceControler.updatephotoAction(req, res);
    });
    /**
   * @api {post} /media-service/photo Save photo
   * @apiName Save photo
   * @apiGroup Media Service
   *
   * @apiSuccess {Object} photo A JSON Object with the url of the photo
   *
   */
    app.post('/media/photo/', (req, res) => {
        mediaServiceControler.savePhoto(req, res);
    });
};
